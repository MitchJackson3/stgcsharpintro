﻿using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ExampleTest
{
    [TestFixture]
    public class Test1
    {
        IWebDriver driver;

        // this method is run before each test
        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
        }

        // this is our test method
        [Test]
        public void TestMethod1()
        {
            string url = "https://www.google.com";
            driver.Navigate().GoToUrl(url);
            Thread.Sleep(5000);
        }

        // this method is run after each test
        [TearDown]
        public void Teardown()
        {
            driver.Quit();
        }
    }
}
